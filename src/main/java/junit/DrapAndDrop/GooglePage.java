package junit.DrapAndDrop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;
import abstractCont.AbstractContainer;

/**
 * Hello world!
 *
 */
public class GooglePage extends AbstractContainer{

	public GooglePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	public GooglePage openGooglePage(){
		driver.get("https://www.google.com");
		return new GooglePage(driver);
	}
	public DrapPage goToDrapPage(){
		((WebElement) driver.findElements(By.xpath("//div[@id='gs_lc0']/input"))).sendKeys("drag and drop examples");
		
		driver.findElement(By.xpath("//button[@class='lsb']")).click();
		return new DrapPage(driver);
		
	}
	}

	


    
